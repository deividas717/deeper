package homework.deeper.com.deeper.events;

/**
 * Created by Deividas on 2017-08-16.
 */

public class DataClickEvent {

    private int id;

    public DataClickEvent(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
