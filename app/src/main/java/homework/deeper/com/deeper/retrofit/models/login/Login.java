package homework.deeper.com.deeper.retrofit.models.login;

/**
 * Created by Deividas on 2017-08-16.
 */

public class Login {
    private String appId;
    private String token;
    private int userId;
    private String validated;
    private long validTill;

    public String getAppId() {
        return appId;
    }

    public String getToken() {
        return token;
    }

    public int getUserId() {
        return userId;
    }

    public String getValidated() {
        return validated;
    }

    public long getValidTill() {
        return validTill;
    }
}
