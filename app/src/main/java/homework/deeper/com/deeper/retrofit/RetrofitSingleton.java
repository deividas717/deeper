package homework.deeper.com.deeper.retrofit;

import homework.deeper.com.deeper.Utils;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Deividas on 2017-08-16.
 */

public class RetrofitSingleton {

    private static RetrofitSingleton instance = null;
    private ApiService service;

    private RetrofitSingleton() {
        buildRetrofit();
    }

    public static RetrofitSingleton getInstance() {
        if (instance == null) {
           instance = new RetrofitSingleton();
        }
        return instance;
    }

    private void buildRetrofit() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpBuilder.build());
        Retrofit retrofit = builder.build();
        service = retrofit.create(ApiService.class);
    }

    public ApiService getService() {
        return service;
    }
}