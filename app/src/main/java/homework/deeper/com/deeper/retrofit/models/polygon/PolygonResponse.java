package homework.deeper.com.deeper.retrofit.models.polygon;

import java.util.List;

/**
 * Created by Deividas on 2017-08-16.
 */

public class PolygonResponse {
    private String type;
    private List<Double> bbox;
    private List<Feature> features;

    public String getType() {
        return type;
    }

    public List<Double> getBbox() {
        return bbox;
    }

    public List<Feature> getFeatures() {
        return features;
    }
}
