package homework.deeper.com.deeper.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import homework.deeper.com.deeper.R;
import homework.deeper.com.deeper.events.DataClickEvent;
import homework.deeper.com.deeper.retrofit.models.login.Scan;

/**
 * Created by Deividas on 2017-08-16.
 */

public class BathymetricDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder > {

    private List<Scan> dataList;

    public BathymetricDataAdapter(List<Scan> dataList) {
        this.dataList = dataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.scan_list_row, parent, false);
        return new ScanItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Scan data = dataList.get(position);
        ScanItemViewHolder listViewHolder = (ScanItemViewHolder) holder;
        listViewHolder.bind(data);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    protected class ScanItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.createdAt)
        TextView createdAt;

        private SimpleDateFormat simpleDateFormat;

        private ScanItemViewHolder(View itemView) {
            super(itemView);

            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.ENGLISH);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new DataClickEvent(dataList.get(getAdapterPosition()).getId()));
                }
            });

            ButterKnife.bind(this, itemView);
        }

        private void bind(Scan scan) {
            String name = scan.getName();
            if (name != null) {
                title.setText(name);
            }
            String formattedDate = simpleDateFormat.format(scan.getDate());
            createdAt.setText(formattedDate);
        }
    }
}
