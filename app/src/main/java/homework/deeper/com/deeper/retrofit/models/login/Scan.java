package homework.deeper.com.deeper.retrofit.models.login;

import org.parceler.Parcel;

/**
 * Created by Deividas on 2017-08-16.
 */

@Parcel
public class Scan {
    private int id;
    private String name;
    private int groupId;
    private long date;
    private int scanPoints;
    private int mode;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getGroupId() {
        return groupId;
    }

    public long getDate() {
        return date;
    }

    public int getScanPoints() {
        return scanPoints;
    }

    public int getMode() {
        return mode;
    }
}
