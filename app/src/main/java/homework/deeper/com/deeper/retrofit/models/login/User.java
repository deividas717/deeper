package homework.deeper.com.deeper.retrofit.models.login;

/**
 * Created by Deividas on 2017-08-16.
 */

public class User {
    private int userId;
    private String familyName;
    private String name;
    private String email;
    private String locale;
    private String subscribe;
    private String image;

    public int getUserId() {
        return userId;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getLocale() {
        return locale;
    }

    public String getSubscribe() {
        return subscribe;
    }

    public String getImage() {
        return image;
    }
}
