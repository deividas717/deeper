package homework.deeper.com.deeper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Deividas on 2017-08-16.
 */

public class Utils {

    public static final String BASE_URL = "https://maps.deepersonar.com/api/";

    /**
     *  method to check if internet is available
     *  @param context Context
     *  @return boolean
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
