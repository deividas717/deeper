package homework.deeper.com.deeper.retrofit;

import homework.deeper.com.deeper.retrofit.models.polygon.PolygonResponse;
import homework.deeper.com.deeper.retrofit.models.login.LoginResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Deividas on 2017-08-16.
 */

public interface ApiService {

    @GET("login?")
    Call<LoginResponse> login(@Query("username") String username, @Query("password") String password);

    @GET("polygon?")
    Call<PolygonResponse> polygon(@Query("token") String token, @Query("scanId") int scanId);
}
