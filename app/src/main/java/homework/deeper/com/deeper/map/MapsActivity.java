package homework.deeper.com.deeper.map;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolygonOptions;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import homework.deeper.com.deeper.R;
import homework.deeper.com.deeper.retrofit.models.polygon.Feature;
import homework.deeper.com.deeper.retrofit.models.polygon.Geometry;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String BOUNDING_BOX = "BOUNDING_BOX";
    public static final String FEATURES = "FEATURES";

    private GoogleMap mMap;
    private List<Double> bbox;
    private List<Feature> features;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        bbox = Parcels.unwrap(getIntent().getParcelableExtra(BOUNDING_BOX));
        features = Parcels.unwrap(getIntent().getParcelableExtra(FEATURES));
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        final LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(bbox.get(0), bbox.get(1)));
        builder.include(new LatLng(bbox.get(2), bbox.get(3)));

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.animateCamera(
                        CameraUpdateFactory.newLatLngBounds(builder.build(), 30),
                        600,
                        null
                );
            }
        });

        displayPolygons();
    }

    /**
     *  method to display polygons
     */
    private void displayPolygons() {
        for (Feature feature : features) {
            Geometry geometry = feature.getGeometry();
            List<LatLng> latLngs = new ArrayList<>();
            double depth = feature.getProperties().getDepth();
            for (List<Double> list : geometry.getCoordinates().get(0)) {
                double lng = list.get(0);
                double lat = list.get(1);
                LatLng latLng = new LatLng(lat, lng);
                latLngs.add(latLng);
            }
            int color = getColor(depth);
            if (color != -1) {
                PolygonOptions options = new PolygonOptions();
                options.strokeWidth(5);
                options.addAll(latLngs);
                options.fillColor(color);
                mMap.addPolygon(options);
            }
        }
    }

    /**
     *  method to get color based on depth
     *  @param depth double
     *  @return int color
     */
    private int getColor(double depth) {
        if (depth <= 1.0) {
            return Color.RED;
        } else if (depth <= 2.0) {
            return Color.GREEN;
        } else if (depth <= 3.0) {
            return Color.BLUE;
        } else if (depth >= 4.0) {
            return Color.YELLOW;
        }
        return -1;
    }
}
