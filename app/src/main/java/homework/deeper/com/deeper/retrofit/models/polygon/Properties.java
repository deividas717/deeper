package homework.deeper.com.deeper.retrofit.models.polygon;

import org.parceler.Parcel;

/**
 * Created by Deividas on 2017-08-16.
 */

@Parcel
public class Properties {
    private double depth;
    private String id;

    public double getDepth() {
        return depth;
    }

    public String getId() {
        return id;
    }
}
