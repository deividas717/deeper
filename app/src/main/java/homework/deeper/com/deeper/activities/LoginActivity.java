package homework.deeper.com.deeper.activities;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;
import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import homework.deeper.com.deeper.R;
import homework.deeper.com.deeper.Utils;
import homework.deeper.com.deeper.retrofit.ApiService;
import homework.deeper.com.deeper.retrofit.RetrofitSingleton;
import homework.deeper.com.deeper.retrofit.models.login.LoginResponse;
import homework.deeper.com.deeper.retrofit.models.login.Scan;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.inputEmail)
    EditText inputEmail;

    @BindView(R.id.inputPassword)
    EditText inputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.loginButton)
    public void login() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), R.string.internet_connection_required,
                    Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Settings.ACTION_SETTINGS));
            return;
        }

        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();

        if (!isValidEmail(email)) {
            Toast.makeText(getApplicationContext(), R.string.email_error, Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.length() == 0) {
            Toast.makeText(getApplicationContext(), R.string.password_error, Toast.LENGTH_SHORT).show();
            return;
        }

        ApiService service = RetrofitSingleton.getInstance().getService();
        Call<LoginResponse> call = service.login(email, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();
                    if (loginResponse != null) {
                        List<Scan> scanList = loginResponse.getScans();
                        Intent intent = new Intent(getApplicationContext(), BathymetricListActivity.class);
                        intent.putExtra(BathymetricListActivity.DATA, Parcels.wrap(scanList));
                        intent.putExtra(BathymetricListActivity.TOKEN, loginResponse.getLogin().getToken());
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.response_error, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     *  method to validate email
     *  @param email String
     *  @return boolean
     */
    private boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
