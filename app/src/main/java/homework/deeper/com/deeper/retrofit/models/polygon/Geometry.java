package homework.deeper.com.deeper.retrofit.models.polygon;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Deividas on 2017-08-16.
 */

@Parcel
public class Geometry {
    private String type;
    private List<Double> bbox;
    private List<List<List<Double>>> coordinates;

    public String getType() {
        return type;
    }

    public List<Double> getBbox() {
        return bbox;
    }

    public List<List<List<Double>>> getCoordinates() {
        return coordinates;
    }
}
