package homework.deeper.com.deeper.retrofit.models.polygon;

import org.parceler.Parcel;

/**
 * Created by Deividas on 2017-08-16.
 */

@Parcel
public class Feature {
    private String type;
    private Properties properties;
    private Geometry geometry;

    public String getType() {
        return type;
    }

    public Properties getProperties() {
        return properties;
    }

    public Geometry getGeometry() {
        return geometry;
    }
}
