package homework.deeper.com.deeper.activities;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import homework.deeper.com.deeper.R;
import homework.deeper.com.deeper.Utils;
import homework.deeper.com.deeper.adapters.BathymetricDataAdapter;
import homework.deeper.com.deeper.events.DataClickEvent;
import homework.deeper.com.deeper.map.MapsActivity;
import homework.deeper.com.deeper.retrofit.ApiService;
import homework.deeper.com.deeper.retrofit.RetrofitSingleton;
import homework.deeper.com.deeper.retrofit.models.login.Scan;
import homework.deeper.com.deeper.retrofit.models.polygon.Feature;
import homework.deeper.com.deeper.retrofit.models.polygon.PolygonResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BathymetricListActivity extends AppCompatActivity {

    public static final String DATA = "DATA";
    public static final String TOKEN = "TOKEN";

    private String token;

    @BindView(R.id.dataListRecyclerView)
    RecyclerView dataListRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bathymetric_list);

        ButterKnife.bind(this);

        token = getIntent().getStringExtra(TOKEN);

        List<Scan> scanList = Parcels.unwrap(getIntent().getParcelableExtra(DATA));

        BathymetricDataAdapter adapter = new BathymetricDataAdapter(scanList);
        dataListRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        dataListRecyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe
    public void onDataSelected(DataClickEvent event) {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), R.string.internet_connection_required, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Settings.ACTION_SETTINGS));
            return;
        }
        ApiService service = RetrofitSingleton.getInstance().getService();
        Call<PolygonResponse> call = service.polygon(token, event.getId());
        call.enqueue(new Callback<PolygonResponse>() {
            @Override
            public void onResponse(Call<PolygonResponse> call, Response<PolygonResponse> response) {
                if (response.isSuccessful()) {
                    PolygonResponse polygonResponse = response.body();
                    if (polygonResponse != null) {
                        List<Double> bbox = polygonResponse.getBbox();
                        List<Feature> features = polygonResponse.getFeatures();

                        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                        intent.putExtra(MapsActivity.BOUNDING_BOX, Parcels.wrap(bbox));
                        intent.putExtra(MapsActivity.FEATURES, Parcels.wrap(features));
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.response_error, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PolygonResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
            }
        });
    }
}