package homework.deeper.com.deeper.retrofit.models.login;

import java.util.List;

import homework.deeper.com.deeper.retrofit.models.login.Login;
import homework.deeper.com.deeper.retrofit.models.login.Manago;
import homework.deeper.com.deeper.retrofit.models.login.Scan;
import homework.deeper.com.deeper.retrofit.models.login.User;

/**
 * Created by Deividas on 2017-08-16.
 */

public class LoginResponse {
    private Login login;
    private User user;
    private List<Scan> scans;
    private Manago manago;

    public Login getLogin() {
        return login;
    }

    public User getUser() {
        return user;
    }

    public List<Scan> getScans() {
        return scans;
    }

    public Manago getManago() {
        return manago;
    }
}
